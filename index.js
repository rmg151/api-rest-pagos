
'use strict' 

const port = process.env.PORT || 8000;

//incorporar librerias:
const https = require('https');
const logger = require('morgan');
const express = require('express');
const mongojs = require('mongojs');
const fs = require('fs');
const passService = require('./services/pass.service');
const tokenService = require('./services/token.service');


const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}; 


//declaramos nuestra aplicacion de tipo express:
const app = express(); 

//variable de mi base de datos:
//var db = mongojs(URL_DB); 
var id = mongojs.ObjectID; //funcion para convertir un id textual en un objeto de mongojs

//declaramos middleware:
app.use(logger('dev'));
app.use(express.urlencoded({extended: false})); //para leer objetos de formularios y poder entender el body de las diferentes r4equest
app.use(express.json()); //para reconocer objetos json

/*
//middleware que cada vez que cambie lo que viene en colecciones, llama a la funcion calllback
app.param("colecciones", (request, response, next, coleccion) => {
    console.log('param /api/:colecciones');
    console.log('coleccion: ', coleccion);

    request.collection = db.collection(coleccion); //creamos puntero a una funcion que apunta a la bd y la tabla indicadas
    return next(); //invocar al siguiente middleware; puntero a la siguiente funcion, o middleware
});
*/

//Autorizacion:
function auth(request, response, next) {
    if (! request.headers.authorization) {
        response.status(401).json({
            result:'KO',
            mensajes: "no has enviado el token en la cabecera"
        })
        return next();
    } 
    
    console.log(request.headers.authorization);

    if( request.headers.authorization.split(" ")[1]){ //token en formato JWT
        return next();
    }

    response.status(401).json({
        result:'KO',
        mensajes: "no has enviado el token en la cabecera"
    })

    return next(new Error("Acceso no autorizado a este servicio. ")); 
}





//Routes y Controllers:


app.put('/api/:colecciones/:id/pagar', auth, (request, response, next) => {
    const nuevoElemento = request.body; 
    var URL_DB;
    var db;

    if(request.params.colecciones == 'vuelos'){
        URL_DB = "mongodb+srv://rmg151:dieciocho@cluster0.6mqfl.mongodb.net/vuelos?retryWrites=true&w=majority";
        db = mongojs(URL_DB);
        request.collection = db.collection(request.params.colecciones);

 
        request.collection.findOne({_id: id(request.params.id)}, (err, coleccion) => {
            if (err) return next(err);
                coleccion.estado="pagado";

                const elemNuevo = coleccion;

                    request.collection.update(
                        {_id: id(request.params.id)}, 
                        {$set: elemNuevo}, //añadir el elemento nuevo
                        {safe: true, multi: false}, (err, elementoModif) => {
                            if (err) return next(err);
                        });

                response.status(201).json({                  
                    vueloPagado: elemNuevo
                });
           });

            }else if(request.params.colecciones == 'hoteles'){
                URL_DB = "mongodb+srv://rmg151:dieciocho@cluster0.6mqfl.mongodb.net/hoteles?retryWrites=true&w=majority";
                db = mongojs(URL_DB);
                request.collection = db.collection(request.params.colecciones);

        
                request.collection.findOne({_id: id(request.params.id)}, (err, coleccion) => {
                    if (err) return next(err);
                        coleccion.estado="pagado";

                        const elemNuevo = coleccion;

                            request.collection.update(
                                {_id: id(request.params.id)}, 
                                {$set: elemNuevo}, //añadir el elemento nuevo
                                {safe: true, multi: false}, (err, elementoModif) => {
                                    if (err) return next(err);
                                });
                        response.status(201).json({                  
                            hotelPagado: elemNuevo
                        });
                });
            }else if(request.params.colecciones == 'coches'){
                URL_DB = "mongodb+srv://rmg151:dieciocho@cluster0.6mqfl.mongodb.net/coches?retryWrites=true&w=majority";
                db = mongojs(URL_DB);
                request.collection = db.collection(request.params.colecciones);

        
                request.collection.findOne({_id: id(request.params.id)}, (err, coleccion) => {
                    if (err) return next(err);
                        coleccion.estado="pagado";

                        const elemNuevo = coleccion;

                            request.collection.update(
                                {_id: id(request.params.id)}, 
                                {$set: elemNuevo}, //añadir el elemento nuevo
                                {safe: true, multi: false}, (err, elementoModif) => {
                                    if (err) return next(err);
                                });
                        response.status(201).json({                  
                            cochePagado: elemNuevo
                        });
                });
            }
});



https.createServer( opciones, app ).listen(port, () => {
    console.log(`API RESTFul CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});





